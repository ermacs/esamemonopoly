# Esecuzione esame Monopoly

In questo repository sono contenute le mie due soluzioni, dotate di test automatici, del problema descritto in [questa pagina](https://schuchert.wikispaces.com/Monopoly%28r%29), in particolare la prima release.

In questo README spiego le scelte generali e le due varianti di implementazione.

## Le varianti
La prima variante è più vicina alla descrizione del problema: ha una struttura che segue i canoni della OOP ed è stata sviluppata con un approccio test-first.

Ho implementato una seconda variante non OOP per mostrare come avrei affrontato il problema senza le direttive architetturali date.

## Considerazioni generali
* Ho dato priorità a leggibilità e manutenibilità.
* Il linguaggio utilizzato è Java con sintassi 1.7.
* Ho scritto il codice in italiano con l'assunzione che tutti i lettori siano italiani.
* Ho utilizzato delle "guardie" per i controlli semantici e validazioni, ma non ho ritenuto necessario fare validazioni sintattiche come la "nullità" degli oggetti.
* Ho commentato dove ritengo che il codice non basti o sia complesso e costoso da leggere.
* Strutturalmente il codice non è pensato per un'evoluzione futura, non essendoci requisiti in merito.

## Variante 1
In base alla descrizione del problema, segue principi della OOP, con oggetti con stato e comportamento che manipola quello stato.

Alcune scelte sono motivabili con pattern GoF o GRASP, ma non ho ritenuto utile formalizzare questo aspetto.

## Variante 2
Questa variante è come l'avrei fatto senza il vincolo della OOP.

Ha una struttura che prevede "comandi che modificano lo stato" e uno stato dedicato ad essi, un "command model".

Un oggetto Comandi espone appunto i "comandi" sotto forma di metodi. Esso per semplicità incapsula il "command model" che utilizza.

Non c'è nessun "view model" che viene aggiornato in funzione dei comandi, perché non c'è nessun requisito che lo richiede. I test accedono in lettura direttamente ai campi dell'oggetto Comandi.

