package var1;

/*
Modella il giocatore e la sua capacità di effettuare una mossa, ossia muoversi lungo un tabellone
attraverso il lancio di dadi.
 */
public class Giocatore {

    Posizione posizioneCorrente;
    String nome;
    Tabellone tabellone;

    public Giocatore(Posizione posizioneIniziale, String nome, Tabellone tabellone) {
        this.posizioneCorrente = posizioneIniziale;
        this.nome = nome;
        this.tabellone = tabellone;
    }

    public Mossa effettuaMossa(){
        Lancio nuovoLancio = Lancio.lancia();
        Posizione posizionePrecedente = posizioneCorrente;
        Posizione nuovaPosizione = tabellone.nuovaPosizioneDopoLancio(posizioneCorrente, nuovoLancio);
        this.posizioneCorrente= nuovaPosizione;
        return new Mossa(this.nome, posizionePrecedente, nuovoLancio.valore(), nuovaPosizione);
    }
}
