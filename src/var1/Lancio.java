package var1;

import java.util.Random;

/*
Rappresenta il lancio di una coppia di dadi.
Può anche essere istanziato con un valore dato.
*/
public class Lancio {

    static Random random = new Random();

    private int valore;

    public Lancio(int v) {
        this.valore = v;
    }

    public static Lancio lancia(){
        return new Lancio(random.nextInt(6) + random.nextInt(6) + 2);
    }

    public int valore(){
        return this.valore;
    }
}
