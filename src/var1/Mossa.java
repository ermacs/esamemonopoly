package var1;

/*
Descrive l'esito del turno di un giocatore.
 */
public class Mossa {
    String nome;
    Posizione posizionePrecedente;
    int valore;
    Posizione nuovaPosizione;
    int numeroGiocatoriRimanenti;

    public Mossa(String nome, Posizione posizionePrecedente, int valore, Posizione nuovaPosizione) {
        this.nome = nome;
        this.posizionePrecedente = posizionePrecedente;
        this.valore = valore;
        this.nuovaPosizione = nuovaPosizione;
    }
}
