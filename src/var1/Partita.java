package var1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Modella la partita e ne implementa i comportamenti principali.
Fa da 'facade' per le azioni relative a turni e mosse del gioco.
*/
class Partita {

    List<Giocatore> giocatori;
    private int indiceGiocatoreCorrente;

    Partita() {
        this.giocatori = new ArrayList<Giocatore>();
    }

    void aggiungiGiocatore(Giocatore giocatore) {
        this.giocatori.add(giocatore);
    }

    void inizia() {
        if (this.giocatori.size() < 2 || this.giocatori.size() >8)
            throw new IllegalStateException("Il numero dei giocatori dev'essere fra 2 e 8 inclusi.");

        Collections.shuffle(this.giocatori);
        indiceGiocatoreCorrente = 0;
    }

    void nuovoRound() {
        indiceGiocatoreCorrente = 0;
    }

    Mossa prossimaMossa() {
        if (indiceGiocatoreCorrente == giocatori.size())
            throw new IllegalStateException("Prima che i giocatori possano muovere va iniziato un nuovo round.");
        Giocatore giocatoreCorrente = giocatori.get(indiceGiocatoreCorrente);
        Mossa mossa = giocatoreCorrente.effettuaMossa();
        mossa.numeroGiocatoriRimanenti = giocatori.size() - indiceGiocatoreCorrente - 1;
        indiceGiocatoreCorrente++;
        return mossa;
    }
}
