package var1;
/*
* Modella il concetto di posizione sul tabellone.
* Se creato correttamente attraverso il tabellone, garantisce di essere sempre valido, ossia
* relativo ad una posizione lecita sul tabellone.
*/
public class Posizione {
    int valore;

    public Posizione(int valore) {
        this.valore = valore;
    }
}
