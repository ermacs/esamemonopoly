package var1;

/*
Descrive un tabellone e calcola una nuova posizione raggiunta a partire da un lancio.
 */
public class Tabellone {
    private final int numeroCaselle;

    public Tabellone(int numeroCaselle) {
        this.numeroCaselle = numeroCaselle;
    }

    public Posizione nuovaPosizioneDopoLancio(Posizione posizionePrecedente, Lancio lancio) {
        int nuovoValore = posizionePrecedente.valore + lancio.valore();
        if (nuovoValore > this.numeroCaselle) {
            nuovoValore = (nuovoValore - this.numeroCaselle);
        }
        return new Posizione(nuovoValore);
    }

    public int numeroCaselle() {
        return numeroCaselle;
    }

    public Posizione nuovaPosizione(int valore) {
        if (valore <0 || valore >= numeroCaselle) throw new IllegalArgumentException("Una posizione deve stare fra 0 e il numero di caselle");
        return new Posizione(valore);
    }
}
