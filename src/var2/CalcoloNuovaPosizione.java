package var2;

/*
Implementa la logica di calcolo di una nuova posizione
su un certo tabellone a partire da un lancio e da un giocatore.
 */
public class CalcoloNuovaPosizione {
    private final int dimensioniTabellone;
    private final Posizione posizioneCorrente;
    private final Lancio lancio;

    public CalcoloNuovaPosizione(Lancio nuovoLancio, Giocatore giocatore, int dimensioniTabellone) {
        this.lancio = nuovoLancio;
        this.posizioneCorrente = giocatore.posizione;
        this.dimensioniTabellone = dimensioniTabellone;
    }

    public Posizione calcola() {
        int nuovoValore = posizioneCorrente.valore + lancio.valore();
        if (nuovoValore >= dimensioniTabellone) nuovoValore = (nuovoValore - dimensioniTabellone);
        return new Posizione(nuovoValore);
    }
}
