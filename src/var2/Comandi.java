package var2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
Espone i comandi, ossia le azioni che possono modificare lo stato.
*/

public class Comandi {
    int dimensioniTabellone;
    int numeroTurniDaGiocare;
    boolean partitaIniziata;
    List<Giocatore> giocatori;
    int indiceGiocatoreCorrente;

    void creaPartita(){
        dimensioniTabellone = 40;
        numeroTurniDaGiocare = 20;
        partitaIniziata = false;
        giocatori = new ArrayList<Giocatore>();
    }

    void aggiungiGiocatore(String nome){
        if (partitaIniziata) throw new IllegalStateException("La partita è iniziata, impossibile aggiungere giocatori");
        giocatori.add(new Giocatore(nome, new Posizione(0)));
    }

    void avviaPartita(){
        if (partitaIniziata) throw new IllegalStateException("La partita è già iniziata");
        if (giocatori.size() < 2 || giocatori.size() > 8) throw new IllegalStateException("Il numero dei giocatori dev'essere fra 2 e 8 inclusi");

        Collections.shuffle(giocatori);
        partitaIniziata = true;
        indiceGiocatoreCorrente = 0;
    }

    void nuovoRound(){
        if (!partitaIniziata) throw new IllegalStateException("La partita è già iniziata");
        indiceGiocatoreCorrente = 0;
    }

    void passaAlSuccessivo(){
        if (!partitaIniziata) throw new IllegalStateException("La partita non è iniziata");
        if (indiceGiocatoreCorrente >= giocatori.size()) throw new IllegalStateException("Va iniziato un nuovo round");
        indiceGiocatoreCorrente++;
    }

    void lancia(){
        if (!partitaIniziata) throw new IllegalStateException("La partita non è iniziata");
        Giocatore giocatore = giocatori.get(indiceGiocatoreCorrente);
        Lancio nuovoLancio = Lancio.lancia();
        giocatore.posizione = new CalcoloNuovaPosizione(nuovoLancio, giocatore, dimensioniTabellone).calcola();
    }



}
