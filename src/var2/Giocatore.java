package var2;

/*
Modella la descrizione del giocatore.
 */
public class Giocatore {
    String nome;
    Posizione posizione;

    public Giocatore(String nome, Posizione posizione) {
        this.nome = nome;
        this.posizione = posizione;
    }
}
