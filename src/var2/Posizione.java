package var2;
/*
Esprime una posizione sul tabellone allo scopo di rafforzare le interfacce dei metodi, in opposizione
 ad int.
*/
public class Posizione {
    int valore;

    public Posizione(int valore) {
        this.valore = valore;
    }
}
