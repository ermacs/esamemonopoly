package var1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestGiocatoriVariante1 {

    /*
    As a game, I can have between 2 and 8 players with an initial random ordering.

    User Acceptance Tests
    Create a game with two players named Horse and Car.
    Try to create a game with < 2 or > 8 players. When attempting to play the game, it will fail.
    Create a game with two players named Horse and Car. Within creating 100 games, both orders [Horse, Car] and [car, horse] occur.
    */

    @Test
    public void dueGiocatoriInPartita(){
        Partita p = new Partita();
        Tabellone t = new Tabellone(40);
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Horse", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Car", t));
        p.inizia();
    }

    @Test(expected = IllegalStateException.class)
    public void menoDiDueGiocatoriInPartita(){
        Partita p = new Partita();
        Tabellone t = new Tabellone(40);
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Alice", t));
        p.inizia();
    }

    @Test(expected = IllegalStateException.class)
    public void piuDiOttoGiocatoriInPartita(){
        Partita p = new Partita();
        Tabellone t = new Tabellone(40);
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Alice", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Bob", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Charlie", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Diego", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Edgar", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Francis", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Ghislaine", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Howard", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0),"Ian", t));
        p.inizia();
    }

    @Test
    public void giocatoriInTuttiGliOrdini(){
        //L'idea qui è contare le volte in cui 'Horse' risulta primo.
        int volteHorsePerPrimo = 0;
        for (int i=0;i<100;i++) {
            Partita p = new Partita();
            Tabellone t = new Tabellone(40);
            p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0), "Horse", t));
            p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0), "Car", t));
            p.inizia();
            if (p.giocatori.get(0).nome.equals("Horse")) volteHorsePerPrimo++;
        }
        int volteCarPerPrimo = 100 - volteHorsePerPrimo;

        assertTrue(volteCarPerPrimo > 0);
        assertTrue(volteHorsePerPrimo > 0);
    }

}

