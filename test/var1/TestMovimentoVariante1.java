package var1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestMovimentoVariante1 {

    /*
    As a player, I can take a turn so that I can move around the board.

    User Acceptance Tests
    Player on beginning location (numbered 0), rolls 7, ends up on location 7
    Player on location numbered 39, rolls 6, ends up on location 5
    */

    @Test
    public void movimentoGiocatori(){
        Tabellone t = new Tabellone(40);
        Posizione nuovaPosizione = t.nuovaPosizioneDopoLancio(t.nuovaPosizione(0), new Lancio(7));
        assertEquals(7,nuovaPosizione.valore);

        nuovaPosizione = t.nuovaPosizioneDopoLancio(t.nuovaPosizione(39), new Lancio(6));
        assertEquals(5,nuovaPosizione.valore);
    }

}

