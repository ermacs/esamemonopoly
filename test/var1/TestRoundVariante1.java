package var1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestRoundVariante1 {

    /*
    As a game, I execute 20 rounds so that I can complete a game.

    User Acceptance Tests
    Create a game and play, verify that the total rounds was 20 and that each player played 20 rounds.
    Create a game and play, verify that in every round the order of the players remained the same.
    */

    @Test
    public void ogniGiocatoreGiocaVentiRound(){
        Partita p = new Partita();
        Tabellone t = new Tabellone(40);
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0), "Horse", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0), "Car", t));
        p.inizia();

        List<Mossa> mosse = new ArrayList<Mossa>();
        for (int i = 0; i < 20;i++) {
            p.nuovoRound();
            mosse.add(p.prossimaMossa());
            mosse.add(p.prossimaMossa());
        }

        int conteggioMosseDiHorse = 0;
        int conteggioMosseDiCars = 0;
        for(Mossa mossa : mosse){
            if (mossa.nome.equals("Horse")) conteggioMosseDiHorse++;
            else conteggioMosseDiCars++;
        }
        assertEquals(40, mosse.size());
        assertEquals(20, conteggioMosseDiHorse);
        assertEquals(20, conteggioMosseDiCars);
    }

    @Test
    public void ogniRoundSiGiocaNelloStessoOrdine(){
        Partita p = new Partita();
        Tabellone t = new Tabellone(40);
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0), "Horse", t));
        p.aggiungiGiocatore(new Giocatore(t.nuovaPosizione(0), "Car", t));
        p.inizia();

        String primoNome = null;
        for (int i = 0; i < 20;i++) {
            p.nuovoRound();
            Mossa primaMossa = p.prossimaMossa();
            if (primoNome == null) primoNome = primaMossa.nome;
            else if (!primoNome.equals(primaMossa.nome)) {
                fail("Atteso "+primoNome+" ma ha mosso "+primaMossa.nome);
            }

            p.prossimaMossa();
        }
    }

}

