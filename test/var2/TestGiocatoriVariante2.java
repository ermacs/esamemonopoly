package var2;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestGiocatoriVariante2 {

    /*
    As a game, I can have between 2 and 8 players with an initial random ordering.

    User Acceptance Tests
    Create a game with two players named Horse and Car.
    Try to create a game with < 2 or > 8 players. When attempting to play the game, it will fail.
    Create a game with two players named Horse and Car. Within creating 100 games, both orders [Horse, Car] and [car, horse] occur.
    */

    @Test
    public void dueGiocatoriInPartita(){
        Comandi c = new Comandi();
        c.creaPartita();
        c.aggiungiGiocatore("Horse");
        c.aggiungiGiocatore("Car");
        c.avviaPartita();
    }

    @Test(expected = IllegalStateException.class)
    public void menoDiDueGiocatoriInPartita(){
        Comandi c = new Comandi();
        c.creaPartita();
        c.aggiungiGiocatore("Alice");
        c.avviaPartita();
    }

    @Test(expected = IllegalStateException.class)
    public void piuDiOttoGiocatoriInPartita(){
        Comandi c = new Comandi();
        c.creaPartita();
        c.aggiungiGiocatore("Alice");
        c.aggiungiGiocatore("Bob");
        c.aggiungiGiocatore("Charlie");
        c.aggiungiGiocatore("Diego");
        c.aggiungiGiocatore("Edgar");
        c.aggiungiGiocatore("Francis");
        c.aggiungiGiocatore("Ghislaine");
        c.aggiungiGiocatore("Howard");
        c.aggiungiGiocatore("Ian");
        c.avviaPartita();
    }

    @Test
    public void giocatoriInTuttiGliOrdini(){
        int volteHorsePerPrimo = 0;
        for (int i=0;i<100;i++) {
            Comandi c = new Comandi();
            c.creaPartita();
            c.aggiungiGiocatore("Horse");
            c.aggiungiGiocatore("Car");
            c.avviaPartita();
            if (c.giocatori.get(0).nome.equals("Horse")) volteHorsePerPrimo++;
        }
        int volteCarPerPrimo = 100 - volteHorsePerPrimo;

        assertTrue(volteCarPerPrimo > 0);
        assertTrue(volteHorsePerPrimo > 0);
    }

}

