package var2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMovimentoVariante2 {

    /*
    As a player, I can take a turn so that I can move around the board.

    User Acceptance Tests
    Player on beginning location (numbered 0), rolls 7, ends up on location 7
    Player on location numbered 39, rolls 6, ends up on location 5
    */

    @Test
    public void movimentoGiocatori(){

        Giocatore g1 = new Giocatore("Alice", new Posizione(0));
        Giocatore g2 = new Giocatore("Bob", new Posizione(39));

        Posizione nuovaPosizione = new CalcoloNuovaPosizione(new Lancio(7), g1, 40).calcola();
        assertEquals(7, nuovaPosizione.valore);

        nuovaPosizione = new CalcoloNuovaPosizione(new Lancio(6), g2, 40).calcola();
        assertEquals(5, nuovaPosizione.valore);
    }

}

