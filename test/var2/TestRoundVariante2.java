package var2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestRoundVariante2 {

    /*
    As a game, I execute 20 rounds so that I can complete a game.

    User Acceptance Tests
    Create a game and play, verify that the total rounds was 20 and that each player played 20 rounds.
    Create a game and play, verify that in every round the order of the players remained the same.
    */




    /*
    quiesto pèerche ho deciso di non mettere un viewmodel dedicato ai test, solo perché Java è scemo con test e visiblità.
     */
    String nomeGiocatoreCorrente(Comandi c){
        int indiceGiocatore = c.indiceGiocatoreCorrente;
        Giocatore g = c.giocatori.get(indiceGiocatore);
        return g.nome;
    }

    @Test
    public void ogniGiocatoreGiocaVentiRound(){
        Comandi c = new Comandi();
        c.creaPartita();
        c.aggiungiGiocatore("Horse");
        c.aggiungiGiocatore("Car");
        c.avviaPartita();

        List<String> nomiGiocatoriCheHannoMosso = new ArrayList<String>();
        for (int i = 0; i < 20;i++) {
            c.nuovoRound();
            c.lancia();
            nomiGiocatoriCheHannoMosso.add(nomeGiocatoreCorrente(c));
            c.passaAlSuccessivo();
            c.lancia();
            nomiGiocatoriCheHannoMosso.add(nomeGiocatoreCorrente(c));
        }

        int conteggioMosseDiHorse = 0;
        int conteggioMosseDiCars = 0;
        for(String nome : nomiGiocatoriCheHannoMosso){
            if (nome.equals("Horse")) conteggioMosseDiHorse++;
            else conteggioMosseDiCars++;
        }
        assertEquals(40, nomiGiocatoriCheHannoMosso.size());
        assertEquals(20, conteggioMosseDiHorse);
        assertEquals(20, conteggioMosseDiCars);
    }

    @Test
    public void ogniRoundSiGiocaNelloStessoOrdine(){
        Comandi c = new Comandi();
        c.creaPartita();
        c.aggiungiGiocatore("Horse");
        c.aggiungiGiocatore("Car");
        c.avviaPartita();

        String primoNome = null;
        for (int i = 0; i < 20;i++) {
            c.nuovoRound();
            c.lancia();
            if (primoNome == null) {
                primoNome = nomeGiocatoreCorrente(c);
            } else {
                String nomeCorrente = nomeGiocatoreCorrente(c);
                if (!primoNome.equals(nomeCorrente)) fail("Atteso "+primoNome+" ma ha mosso "+nomeCorrente);
            }
            c.passaAlSuccessivo();
            c.lancia();
        }

    }

}

